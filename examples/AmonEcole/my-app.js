import { Config } from 'tiramisu-json-api.js';
import { TiramisuWebUI } from '../../tiramisu-web-ui';
import { LitElement, html, css } from '../../lit-element/lit-element.js';

class MyApp extends LitElement {
  static get properties() {
    return {
      config: { type: Object },
      _page: { type: String }
    };
  }

  static get styles() {
    return [
      css`
        :host {
          display: block;
          padding: 24px;
          max-width: 600px;
        }

        header {
          display: flex;
          flex-direction: column;
          align-items: center;
        }

        /* Workaround for IE11 displaying <main> as inline */
        main {
          display: block;
        }

        .page {
          display: none;
        }

        .page[active] {
          display: block;
        }

        footer {
          border-top: 1px solid #ccc;
          text-align: center;
        }

        /* Wide layout */
        @media (min-width: 460px) {
          header {
            flex-direction: row;
          }

          /* The drawer button isn't shown in the wide layout, so we don't
          need to offset the title */
          [main-title] {
            padding-right: 0px;
          }
        }
      `
    ];
  }

  render() {
    // Anything that's related to rendering should be done in here.
    if(this.config) {
      return html`
        <header>
          <tiramisu-web-ui .config=${this.config}
                           width='800px'
                           fullWidth>
          </tiramisu-web-ui>
        </header>
      `;
    }
  }

  firstUpdated() {
    let request = new XMLHttpRequest();
    request.open('GET', 'examples/AmonEcole/variables.json', false);
    request.send(null);
    let json = JSON.parse(request.responseText);
    this.config = new Config(json);
  }

  stateChanged(state) {
    this._page = state.app.page;
  }
}

window.customElements.define('my-app', MyApp);
