/**
@license
Copyright (C) 2019 Team tiramisu (see AUTHORS for all contributors)

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
____________________________________________________________
*/

import { LitElement, html } from 'lit-element';
import { TiramisuOption } from 'tiramisu-option.js';

class TiramisuOptionDescription extends LitElement {
  constructor() {
    super(...arguments);
    this.children_names = Array();
    this.currentupdate = false;
    // this.width = '380px';
    // this.fullWidth = false;
  }
  static get properties() {
    return { path: String,
             title: {'type': String },
             fullWidth: {'type': Boolean },
             width: String };
  }
  async new_option_avalaible(path) {
    this.currentupdate = true;
    this.update();
    await this.updateComplete;
    this.currentupdate = false;
  }
  firstUpdated() {
    super.firstUpdated();
    const self = this;
    document.addEventListener('tiramisu_change',
                              function(evt) {
                                const path = evt.detail.path;
                                // if suboption
                                if(path.startsWith(self.path + '.') && path.slice(self.path.length + 1).indexOf('.') === -1) {
                                  if(evt.detail['change']['hidden'] === false) {
                                    if(self.children_names.indexOf(path) === -1) {
                                      self.new_option_avalaible(path);
                                    }
                                  }
                                }
                              });
  }
  valueChange(evt) {
    if(this.currentupdate === false) {
      const value = evt.detail.value;
      const path = evt.detail.path;
      //console.log('valueChange', path, value);
      this.config.option(path).value.set(value);
    }
  }
  render() {
    this.children_names = Array();
    let subconfig;
    let first_level = false;
    if(this.subconfig !== undefined) {
      subconfig = this.subconfig;
    } else {
      subconfig = this.config.option;
      first_level = true;
    }
    const itemTemplates = [];
    for (const subsubconfig of subconfig.list()) {
      this.children_names.push(subsubconfig.option.path());
      if(subsubconfig.option.isoptiondescription()) {
        const subtitle = subsubconfig.option.doc();
        itemTemplates.push(html`<tiramisu-optiondescription path="${subsubconfig.option.path()}"
                                                            title="${subtitle}"
                                                            .config=${this.config}
                                                            .subconfig=${subsubconfig}
                                                            ?fullWidth=${this.fullWidth}
                                                            width=${this.width}>
                                </tiramisu-optiondescription>`)
      } else {
        const type = subsubconfig.option.type();
        let list;
        if(type === 'choice') {
          list = subsubconfig.value.list();
        } else {
          list = null;
        }
        const path = subsubconfig.option.path();
        let value = subsubconfig.value.get()
        if(value === null) {
          value = '';
        }
        itemTemplates.push(html`<tiramisu-option type="${type}"
                                                 path="${path}"
                                                 doc="${subsubconfig.option.doc()}"
                                                 value=${value}
                                                 @change=${this.valueChange}
                                                 .list=${list}
                                                 ?fullWidth=${this.fullWidth}
                                                 width=${this.width}>
                                </tiramisu-option>`)
      }
    }
    let title;
    if(this.title.length === 0) {
      title = '';
    } else {
      title = html`<h2>${this.title}</h2>`;
    }
    return html`
      ${title}
      ${itemTemplates}
    `;
  }
}

window.customElements.define('tiramisu-optiondescription', TiramisuOptionDescription);
