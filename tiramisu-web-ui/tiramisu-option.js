/**
@license
Copyright (C) 2019 Team tiramisu (see AUTHORS for all contributors)

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
____________________________________________________________
*/

import { LitElement, html } from 'lit-element/lit-element.js';
import { TextField } from "@authentic/mwc-textfield/mwc-textfield.js"
import { Select } from '@authentic/mwc-select/mwc-select.js'
import { emit } from '@authentic/mwc-base/utils';
//import { Checkbox } from "@material/mwc-checkbox"
import { Switch } from "@authentic/mwc-switch/mwc-switch.js"

class TiramisuSelect extends Select {
  constructor() {
    super(...arguments);
    this.emit_change = false;
  }
  get value() {
    return super.value;
  }
  set value(value) {
    super.value = value;
    if(this.emit_change) {
      emit(this, 'change', {'value': value, 'path': this.id});
    }
  }
  firstUpdated() {
    super.firstUpdated();
    const self = this;
    document.addEventListener('tiramisu_change',
                              function(evt) {
                                if(self.id === evt.detail.path) {
                                  if('value' in evt.detail['change']) {
                                    self.value = evt.detail['change']['value'];
                                  }
                                }
                              });
  }
  updated() {
    super.updated(...arguments);
    this.emit_change = true;
  }
}
window.customElements.define('tiramisu-select', TiramisuSelect);

class TiramisuTextField extends TextField {
  constructor() {
    super(...arguments);
    this.emit_change = false;
  }
  get hasLabel() {
    return this.label;
  }
  get value() {
    return super.value;
  }
  set value(value) {
    super.value = value;
    if(this.emit_change) {
      emit(this, 'change', {'value': value, 'path': this.id});
    }
  }
  firstUpdated() {
    super.firstUpdated();
    this.emit_change = true;
    const self = this;
    document.addEventListener('tiramisu_change',
                              function(evt) {
                                if(self.id === evt.detail.path) {
                                  const change = evt.detail['change'];
                                  if('hidden' in change) {
                                    emit(self, 'hidden', change['hidden']);
                                  }
                                  if('value' in change) {
                                    self.value = change['value'];
                                  }
                                }
                              });
  }
}
window.customElements.define('tiramisu-textfield', TiramisuTextField);

class TiramisuSwitch extends Switch {
  constructor() {
    super(...arguments);
    this.emit_change = false;
  }
  get checked() {
    return super.value;
  }
  set checked(value) {
    super.checked = value;
    if(this.emit_change) {
      emit(this, 'change', {'value': value, 'path': this.id});
    }
  }
  firstUpdated() {
    super.firstUpdated();
    this.emit_change = true;
    const self = this;
    document.addEventListener('tiramisu_change',
                              function(evt) {
                                if(self.id === evt.detail.path) {
                                  const change = evt.detail['change'];
                                  if('hidden' in change) {
                                    emit(self, 'hidden', change['hidden']);
                                  }
                                  if('value' in change) {
                                    self.checked = change['value'];
                                  }
                                }
                              });
  }
}
window.customElements.define('tiramisu-switch', TiramisuSwitch);

class TiramisuOption extends LitElement {
  static get properties() {
    return { type: String,
             path: String,
             doc: String,
             value: Object,
             hidden: { 'type': Boolean, 'value': false },
             list: Array,
             fullWidth: { 'type': Boolean },
             width: String };
  }
  valueChange(evt) {
    //mwc-select send event with evt without detail
    if(evt.detail) {
      emit(this, 'change', { 'value': evt.detail.value, 'path': this.path });
    }
  }
  render() {
    super.render();
    let elmt;
    const self = this;
    switch (this.type) {
      case 'str':
      case 'domainname':
        elmt = html`<tiramisu-textfield id="${this.path}"
                                        style="width: ${this.width};"
                                        title="${this.doc}"
                                        label="${this.doc}"
                                        @change=${this.valueChange}
                                        @hidden=${this.hiddenElmt}
                                        value=${this.value}
                                        ?fullWidth=${this.fullWidth}>
                    </tiramisu-textfield>`;
        break;
      case 'int':
        elmt = html`<tiramisu-textfield id="${this.path}"
                                        style="width: ${this.width};"
                                        title="${this.doc}"
                                        @change=${this.valueChange}
                                        @hidden=${this.hiddenElmt}
                                        label="${this.doc}"
                                        type="number"
                                        value=${this.value}
                                        ?fullWidth=${this.fullWidth}>
                    </tiramisu-textfield>`;
        break;
      case 'password':
        elmt = html`<tiramisu-textfield id="${this.path}"
                                        style="width: ${this.width};"
                                        title="${this.doc}"
                                        label="${this.doc}"
                                        @change=${this.valueChange}
                                        @hidden=${this.hiddenElmt}
                                        type="password"
                                        value=${this.value}
                                        ?fullWidth=${this.fullWidth}>
                    </tiramisu-textfield>`;
        break;
      case 'choice':
        const list = [];
        let selectedIndex = -1
        for(const idx in this.list) {
          const val = this.list[idx];
          if(this.value === val) {
            selectedIndex = parseInt(idx);
          }
          list.push(html`<option style="width: calc(${this.width} - 64px);" value=${val}>${val}</option>`);
        }
        if(this.path === 'creole.systeme.activer_ctrl_alt_suppr') {
          console.log(this.path, selectedIndex);
        }
        elmt = html`<tiramisu-select label="${this.doc}"
                                     @change=${this.valueChange}
                                     @hidden=${this.hiddenElmt}
                                     selectedIndex=1
                                     style="width: ${this.width}"
                                     title="${this.doc}"
                                     ?fullWidth=${this.fullWidth}>
          <select slot="select">
            ${list}
          </select>
        </tiramisu-select>`;
        break;
      case 'bool':
        elmt = html`${this.doc}
                    <tiramisu-switch id="${this.path}"
                                     style="width: ${this.width};"
                                     title="${this.doc}"
                                     @change=${this.valueChange}
                                     @hidden=${this.hiddenElmt}
                                     type="password"
                                     value=${this.value}
                                     ?fullWidth=${this.fullWidth}>
                    </tiramisu-switch>`
                                     
        break;
      default:
        throw `unsupported type ${this.type} for ${this.path}`;
    }
    return html`<div ?hidden="${this.hidden}">
                  ${elmt}
                </div>`;
  }
  hiddenElmt(evt) {
    this.hidden = evt.detail;
  }
  updated() {
    super.updated(...arguments);
    this.emit_change = true;
  }
}

window.customElements.define('tiramisu-option', TiramisuOption);
