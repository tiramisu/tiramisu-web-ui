/**
@license
Copyright (C) 2019 Team tiramisu (see AUTHORS for all contributors)

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
____________________________________________________________
*/

import { LitElement, html } from 'lit-element';
import { TiramisuOptionDescription } from 'tiramisu-optiondescription.js';

class TiramisuWebUI extends LitElement {
  static get properties() {
    return { fullWidth: {'type': Boolean, 'value': '380px' },
             width: { 'type': String, 'value': false } };
  }
  render() {
    if(this.config) {
      const subconfig = this.config.option;
      const lst = subconfig.list();
      const spath = lst[Object.keys(lst)[0]].option.path().split('.');
      let path;
      if(spath.length > 1) {
        path = spath.slice(0, spath.length - 1).join('.')
      } else {
        path = '';
      }
      const title = '';
      return html`<tiramisu-optiondescription path="${path}"
                                              title="${title}"
                                              .config=${this.config}
                                              .subconfig=${subconfig}
                                              ?fullWidth=${this.fullWidth}
                                              width=${this.width}>
                  </tiramisu-optiondescription>`;
    }
  }
}

window.customElements.define('tiramisu-web-ui', TiramisuWebUI);
